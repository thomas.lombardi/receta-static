import React from 'react';
import PropTypes from 'prop-types';

const EditRecipeComponent = ({
  title, updateRecipe, onChangeRating, newRating, editRecipe
}) => (
  <div>
    <input type="text" value={newRating} onChange={onChangeRating} />
    <button onClick={() => updateRecipe(title, newRating)}>
      SAVE
    </button>
    <button onClick={() => editRecipe(title)}>
      CANCEL
    </button>
  </div>
);


EditRecipeComponent.propTypes = {
  newRating: PropTypes.string.isRequired,
  editRecipe: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  updateRecipe: PropTypes.func.isRequired,
  onChangeRating: PropTypes.func.isRequired
};

export default EditRecipeComponent;