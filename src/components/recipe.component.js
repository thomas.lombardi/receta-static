import React from 'react';
import PropTypes from 'prop-types';

import EditRecipeComponent from './edit-recipe.component';
import ViewRecipeComponent from './view-recipe.component';

class RecipeComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      rating: props.rating
    };
  }

  onChangeRating = (event) => {
    this.setState({ rating: event.target.value });
  }

  render() {
    const containerStyle = {
      maxWidth: '300px',
      textAlign: 'auto',
      marginLeft: '40px'
    };

    return (
      <div style={containerStyle}>
        <h4>{this.props.title}</h4>
        { this.props.editMode
          ? <EditRecipeComponent {...this.props} newRating={this.state.rating} onChangeRating={this.onChangeRating} />
          : <ViewRecipeComponent {...this.props} />
        }
        <hr />
      </div>
    );
  }
}

RecipeComponent.propTypes = {
  title: PropTypes.string,
  rating: PropTypes.string,
  editMode: PropTypes.bool,
  editRecipe: PropTypes.func.isRequired,
  updateRecipe: PropTypes.func.isRequired,
};

RecipeComponent.defaultProps = {
  title: '',
  rating: '',
  editMode: false
};

export default RecipeComponent;