import React from 'react';
import PropTypes from 'prop-types';

const recStyle = {
  display: 'flex',
  justifyContent: 'space-between'
};

const ViewRecipeComponent = ({ rating, editRecipe, title }) => (
  <div style={recStyle}>
    <span>RATING: {rating}</span>
    <button onClick={() => editRecipe(title)}>
      Edit
    </button>
  </div>
);

ViewRecipeComponent.propTypes = {
  rating: PropTypes.string.isRequired,
  editRecipe: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired
};

export default ViewRecipeComponent;