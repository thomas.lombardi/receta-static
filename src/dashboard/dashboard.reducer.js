import _ from 'lodash';
import { DashboardActions } from './dashboard.actions';

const initialState = {
  featuredRecipes: [],
};

const DashboardReducer = (state = initialState, action) => {
  switch (action.type) {
    case DashboardActions.GET_STARTING_DATA: {
      return {
        ...state,
        featuredRecipes: action.payload,
      };
    }

    case DashboardActions.TOGGLE_EDIT: {
      const idx = _.findIndex(state.featuredRecipes, { title: action.payload });
      return {
        ...state,
        featuredRecipes: [
          ...state.featuredRecipes.slice(0, idx),
          { ...state.featuredRecipes[idx], editMode: !state.featuredRecipes[idx].editMode },
          ...state.featuredRecipes.slice(idx + 1)
        ],
      };
    }

    case DashboardActions.UPDATE_RECIPE: {
      const idx = _.findIndex(state.featuredRecipes, { title: action.payload.title });
      return {
        ...state,
        featuredRecipes: [
          ...state.featuredRecipes.slice(0, idx),
          { ...state.featuredRecipes[idx], editMode: false, rating: action.payload.rating },
          ...state.featuredRecipes.slice(idx + 1)
        ]
      };
    }

    default: {
      return state;
    }
  }
};

export default DashboardReducer;
