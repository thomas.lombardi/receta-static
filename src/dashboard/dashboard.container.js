import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import store from '../store';
import { getStartingData, flagForEdit, updateRecipe } from './dashboard.actions';
import Recipe from '../components/recipe.component';


class DashboardContainer extends React.Component {
  componentDidMount() {
    store.dispatch(getStartingData());
  }

  editRecipe = (title) => {
    store.dispatch(flagForEdit(title));
  }

  updateRecipe = (title, rating) => {
    store.dispatch(updateRecipe(title, rating));
  }

  renderFeaturedRecipes() {
    return this.props.dashboard.featuredRecipes.map((r) => {
      const recProps = {
        key: r.title,
        title: r.title,
        rating: r.rating,
        editMode: r.editMode,
        editRecipe: this.editRecipe,
        updateRecipe: this.updateRecipe
      };
      return (<Recipe {...recProps} />);
    });
  }

  render() {
    return (
      <div>
        <h3>Featured Recipes</h3>
        {this.renderFeaturedRecipes()}
      </div>
    );
  }
}

DashboardContainer.propTypes = {
  dashboard: PropTypes.shape({
    featuredRecipes: PropTypes.array,
  }),
};

DashboardContainer.defaultProps = {
  dashboard: {
    featuredRecipes: [],
  },
};

const mapStateToProps = stor => ({
  dashboard: stor.dashboard,
});

export default connect(mapStateToProps)(DashboardContainer);
