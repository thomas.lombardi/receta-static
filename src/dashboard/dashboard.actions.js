import recipes from '../api/recipes.json';

export const DashboardActions = {
  GET_STARTING_DATA: 'GET_STARTING_DATA',
  TOGGLE_EDIT: 'TOGGLE_EDIT',
  UPDATE_RECIPE: 'UPDATE_RECIPE',
  CANCEL_UPDATE: 'CANCEL_UPDATE'
};

export function getStartingData() {
  return {
    type: DashboardActions.GET_STARTING_DATA,
    payload: recipes,
  };
}

export function flagForEdit(title) {
  return {
    type: DashboardActions.TOGGLE_EDIT,
    payload: title
  };
}

export function updateRecipe(title, rating) {
  return {
    type: DashboardActions.UPDATE_RECIPE,
    payload: { title, rating }
  };
}

export function cancelUpdate(title) {
  return {
    type: DashboardActions.CANCEL_UPDATE,
    payload: title
  };
}