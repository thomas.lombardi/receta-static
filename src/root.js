import { combineReducers } from 'redux';

import DashboardReducer from './dashboard/dashboard.reducer';

export default combineReducers({
  dashboard: DashboardReducer
});