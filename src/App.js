import React from 'react';
import { Provider } from 'react-redux';
import store from './store';
import './App.css';
import DashboardContainer from './dashboard/dashboard.container';

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <div className="App">
          <DashboardContainer />
        </div>
      </Provider>
    );
  }
}

export default App;
